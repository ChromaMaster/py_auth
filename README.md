# Auth

Auth is a thouth as a microservice that allows your app to login using a shared
cache database

## Table of Contents

- [Auth](#auth)
    - [Table of Contents](#table-of-contents)
    - [Getting Started](#getting-started)
        - [Prerequisites](#prerequisites)
        - [Installing](#installing)
    - [Running the tests](#running-the-tests)
        - [Break down into end to end tests](#break-down-into-end-to-end-tests)
        - [And coding style tests](#and-coding-style-tests)
    - [Deployment](#deployment)
    - [Built With](#built-with)
    - [Contributing](#contributing)
    - [Versioning](#versioning)
    - [Authors](#authors)
    - [License](#license)
    - [Acknowledgments](#acknowledgments)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

**[Back to top](#table-of-contents)**

### Prerequisites

This app is currently using python 3.6.4 and the following packages will be needed to run it.

- flask
- pyyaml
- sqlalchemy
- redis
- requests
- pyjwt
- mysqlclient: You will have to follow the instruction to install [here](https://pypi.org/project/mysqlclient/)

What things you need to install the software and how to install them

**[Back to top](#table-of-contents)**

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

**[Back to top](#table-of-contents)**

## Running the tests

Explain how to run the automated tests for this system

**[Back to top](#table-of-contents)**

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

**[Back to top](#table-of-contents)**

### And coding style tests

Explain what these tests test and why

```
Give an example
```

**[Back to top](#table-of-contents)**

## Deployment

Add additional notes about how to deploy this on a live system

**[Back to top](#table-of-contents)**

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

**[Back to top](#table-of-contents)**

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

**[Back to top](#table-of-contents)**

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

**[Back to top](#table-of-contents)**

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

**[Back to top](#table-of-contents)**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

**[Back to top](#table-of-contents)**

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

**[Back to top](#table-of-contents)**
