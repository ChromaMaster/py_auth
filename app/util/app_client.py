import json

import requests
from requests.auth import HTTPBasicAuth


class Api_client:
    """ Connect to a http/https api
Authentication will be made through basic-auth
"""

    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.protocol = "https" if self.port == 443 else "http"
        self.base_url = "{}://{}:{}".format(self.protocol,
                                            self.host, self.port)
        self.token = ""
        self.base_headers = {"Content-type": "application/json",
                             "Accept": "application/json",
                             "x-access-token": ""}

    def login(self, user, password):
        """Login and get the token"""
        url = "{}/{}".format(self.base_url, "login")

        # TODO: ConnectionRefusedException could be raised
        res = requests.get(url, auth=HTTPBasicAuth(user, password))
        # If status is ok, keep the token
        if res.status_code == 200:
            token = res.json()['token']
            self.base_headers["x-access-token"] = token

        return res.status_code, res.json()

    def logout(self):
        """Logout from the api"""
        url = "{}/{}".format(self.base_url, "login/logout")
        res = requests.get(url, headers=self.base_headers)
        return res.status_code, res.json()

    def get(self, endpoint=""):
        """Makes an get request to the endpoint"""
        url = "{}/{}".format(self.base_url, endpoint)
        res = requests.get(url, headers=self.base_headers)
        return res.status_code, res.json()

    def post(self, endpoint="", data={}):
        """Makes an post request to the endpoint"""
        url = "{}/{}".format(self.base_url, endpoint)
        res = requests.post(url, headers=self.base_headers,
                            data=json.dumps(data))
        return res.status_code, res.json()

    def put(self, endpoint="", data=""):
        """Makes an put request to the endpoint, data will be placed in the url"""
        url = "{}/{}/{}".format(self.base_url, endpoint, data)
        res = requests.put(url, headers=self.base_headers)
        return res.status_code, res.json()

    def delete(self, endpoint="", data=""):
        """Makes an delete request to the endpoint, data will be placed in the url"""
        url = "{}/{}/{}".format(self.base_url, endpoint, data)
        res = requests.delete(url, headers=self.base_headers)
        return res.status_code, res.json()

    @staticmethod
    def json_pretty(json_obj, sort_keys=True, indent=4):
        """Static method that 'prettyfy' the JSON"""
        return json.dumps(json_obj, sort_keys=sort_keys, indent=indent)
