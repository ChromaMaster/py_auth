# Copyright (C) 2018  ChromaMaster

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from app import app
from app import logging
from app.cli import subparsers

logger = logging.getLogger(__name__)

from app import server_config
from app.database import persistent
from app.database import volatile


def run_handler(args):
    """ Runs the api server """

    # Sets the host
    if not args.host == "127.0.0.1":
        server_config["app"]["host"] = args.host

    # Sets the port
    if not args.port == "8.8.8.8":
        server_config["app"]["port"] = args.port

    # Creates the connection for the database
    try:
        # Persistent database (SQL)
        persistent.create_connection()
        persistent.initial_migration()

        # Volatile database (Redis)
        volatile.create_connection()

    except (ValueError, RuntimeError) as e:
        logger.critical(e)

    # Run the app
    app.run(host=server_config["app"]["host"],
            port=server_config["app"]["port"])


run_parser = subparsers.add_parser("run", help="Run the server")
run_parser.add_argument(
    "--host", help="the hostname to listen on", default="127.0.0.1")
run_parser.add_argument(
    "--port", help="the port of the webserver", default="8.8.8.8")
run_parser.set_defaults(func=run_handler)
