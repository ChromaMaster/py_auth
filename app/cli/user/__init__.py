# Copyright (C) 2018  ChromaMaster

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from app.cli import subparsers

user_group = subparsers.add_parser("user", help="users management")


def user_handler(args):
    """ Prints the help if its called without arguments """
    user_group.print_help()


user_group.set_defaults(func=user_handler)

user_subparser = user_group.add_subparsers(
    title="Users subcommands", description="Manage users data")

from . import create_command
from . import import_command
from . import export_command
