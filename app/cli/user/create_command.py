# Copyright (C) 2018  ChromaMaster

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import json

from . import user_subparser
from app import client_config
from app import logging
logger = logging.getLogger(__name__)

from app.database.persistent.model.user import User
from app.database.persistent import create_user

from app.util.app_client import Api_client


def create_handler(args):
    client = Api_client(
        client_config["auth_server"]["host"], client_config["auth_server"]["port"])

    # TODO: Nested exceptions must be treated
    try:
        res_code, _ = client.login(
            client_config["username"], client_config["password"])
    except ConnectionRefusedError:
        print("Connection refused")

    # Logged in
    if res_code == 200:
        new_user = {
            "username": args.username,
            "password": args.password
        }

        res_code, res_data = client.post(endpoint="register", data=new_user)

        # New user successfully registered
        if res_code == 200:
            print(res_code)
        else:
            logger.critical("{} -- {}".format(res_code, res_code))
    else:
        logger.critical("{}".format(res_code))
    res_code, res_data = client.get("register")


create_parser = user_subparser.add_parser('create', help='Create a new user')

create_parser.add_argument(
    "-u", "--username", help="user's username", required=True)
create_parser.add_argument(
    "-p", "--password", help="user's password", required=True)
create_parser.set_defaults(func=create_handler)
