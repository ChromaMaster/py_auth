# Copyright (C) 2018  ChromaMaster

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import json

from . import user_subparser
from app.database.persistent.model.user import User
from app.database.persistent import import_users


def export_handler(args):
    with open(args.file, "w") as file:
        pass
        # data = json.load(file)
        # print(data)


export_parser = user_subparser.add_parser('export', help='export subcommand')

export_parser.add_argument("file", help="path of the file to be exported")

export_parser.set_defaults(func=export_handler)
