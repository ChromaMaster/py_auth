# Copyright (C) 2018  ChromaMaster

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import jwt
from flask import jsonify
from flask import request
from jwt.exceptions import InvalidSignatureError

from app import app
from app.database import volatile


@app.route("/logout", methods=["GET"])
def logout():
    # Looks for the user token
    try:
        token = request.headers["x-access-token"]
    except KeyError:
        return jsonify({"message": "token_is_missing"}), 401

    # Decode token to get user public_id and figure out if token is valid
    try:
        data = jwt.decode(token, app.config["SECRET_KEY"])
    except InvalidSignatureError:
        return jsonify({"message": "token_is_invalid"}), 401

    # Check out if user is logged in
    user = volatile.get_user(data["public_id"])
    if not user:
        return jsonify({"message": "user_not_logged_in"}), 400

    # Remove the user session from sessions server
    if not volatile.remove_user(data["public_id"]):
        return jsonify({"message": "failed_to_logged_out"}), 400

    return jsonify({"message": "logged_out_succesfully"}), 200
