# Copyright (C) 2018  ChromaMaster

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import hashlib
import uuid

from flask import jsonify
from flask import request
from sqlalchemy.exc import IntegrityError

from app import app
from app.database.persistent import create_user
from app.database.persistent.model.user import User


@app.route("/register", methods=["POST"])
def register():
    # Get the data from the request body
    data = request.get_json()

    # No username and password in the request body
    if "username" not in data or "password" not in data:
        return jsonify({"message": "data_missing_or_bad_fields"}), 400

    # Create the password
    hashed_password = hashlib.new("sha256", data["password"].encode("UTF-8"))

    # Create the new user
    new_user = User(public_id=str(uuid.uuid4()),
                    role=5,
                    username=data["username"],
                    password=hashed_password.hexdigest())

    # Creates the user
    try:
        create_user(new_user)
    except IntegrityError:
        return jsonify({"message": "username_already_exists"}), 400

    return jsonify({"message": "Successfully registered"}), 200
