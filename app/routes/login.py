# Copyright (C) 2018  ChromaMaster

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import hashlib
import time

import jwt
from flask import jsonify
from flask import request

from app import app
from app import server_config
from app.database import volatile
from app.database.persistent import get_user_by_username


@app.route("/login", methods=["GET"])
def login():
    """ Logs the user in creating a session with his data """
    auth = request.authorization

    # Username or password not present
    if not auth or not auth.username or not auth.password:
        return jsonify({"message": "could_not_verify"}), 401

    # Get the user that matches the username
    user = get_user_by_username(auth.username)
    if not user:
        return jsonify({"message": "username_does_not_exists"}), 401

    # Check if the password match
    hashed_password = hashlib.new(
        "sha256", auth.password.encode("UTF-8")).hexdigest()
    if not user.password == hashed_password:
        return jsonify({"message": "password_does_not_match"}), 401

    # Create the token
    token = jwt.encode({"public_id": user.public_id,
                        "issued_at": str(time.time())},
                       # "issued_at": ""},
                       server_config["app"]["secret_key"])
    decoded_token = token.decode("UTF-8")

    # Creates the user session
    volatile.create_user(decoded_token, user)

    return jsonify({"message": "Logged in succesfully!",
                    "token": decoded_token}), 200
