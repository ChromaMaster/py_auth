# Copyright (C) 2018  ChromaMaster

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.CRITICAL)
logger = logging.getLogger(__name__)

import yaml
import os

from flask import Flask
from flask import jsonify

app = Flask(__name__)

#########################################
# SERVER CONFIG
#########################################
_server_config_file = "etc/server-config.yaml"

# Default server configuration
server_config = {
    "app": {
        "port": 8888,
        "secret_key": ""
    },
    "database": {
        "persistent": {
            "type": "mysql",
            "host": "127.0.0.1",
            "port": 3306,
            "database": "auth",
            "username": "root",
            "password": "default",
            # SQLite - path to database
            "storage": "app.sqlite3"
        },
        "volatile": {
            "type": "redis",
            "host": "localhost",
            "port": 6379,
            "database": 0,
            "username": "redis",
            "password": "",
            # Time that data will be in the data base (minutes)
            "expiration_time": 10
        }
    }
}

# If config file is provided for server, use that instead of default
if os.path.exists(_server_config_file):
    with open(_server_config_file, "r") as config_file:
        server_config = yaml.load(config_file)

# If environment variables provided for server, use that instead of default or file
ENV_SERVER_SECRET_KEY = os.environ.get("AUTH_SERVER_SECRET_KEY", default=None)
if ENV_SERVER_SECRET_KEY is not None:
    server_config["app"]["secret_key"] = ENV_SERVER_SECRET_KEY

ENV_SERVER_PORT = os.environ.get("AUTH_SERVER_PORT", default=None)
if ENV_SERVER_SECRET_KEY is not None:
    server_config["app"]["secret_key"] = ENV_SERVER_PORT

# Persistent database
ENV_SERVER_DB_TYPE = os.environ.get("AUTH_SERVER_DB_TYPE", default=None)
if ENV_SERVER_DB_TYPE is not None:
    server_config["database"]["persistent"]["type"] = ENV_SERVER_DB_TYPE

ENV_SERVER_DB_HOST = os.environ.get("AUTH_SERVER_DB_HOST", default=None)
if ENV_SERVER_DB_HOST is not None:
    server_config["database"]["persistent"]["host"] = ENV_SERVER_DB_HOST

ENV_SERVER_DB_PORT = os.environ.get("AUTH_SERVER_DB_PORT", default=None)
if ENV_SERVER_DB_PORT is not None:
    server_config["database"]["persistent"]["port"] = ENV_SERVER_DB_PORT

ENV_SERVER_DB_DATABASE = os.environ.get(
    "AUTH_SERVER_DB_DATABASE", default=None)
if ENV_SERVER_DB_DATABASE is not None:
    server_config["database"]["persistent"]["database"] = ENV_SERVER_DB_DATABASE

ENV_SERVER_DB_USERNAME = os.environ.get(
    "AUTH_SERVER_DB_USERNAME", default=None)
if ENV_SERVER_DB_USERNAME is not None:
    server_config["database"]["persistent"]["username"] = ENV_SERVER_DB_USERNAME

ENV_SERVER_DB_PASSWORD = os.environ.get(
    "AUTH_SERVER_DB_PASSWORD", default=None)
if ENV_SERVER_DB_PASSWORD is not None:
    server_config["database"]["persistent"]["password"] = ENV_SERVER_DB_PASSWORD

ENV_SERVER_DB_STORAGE = os.environ.get("AUTH_SERVER_DB_STORAGE", default=None)
if ENV_SERVER_DB_STORAGE is not None:
    server_config["database"]["persistent"]["storage"] = ENV_SERVER_DB_STORAGE

# Volatile database
ENV_SERVER_DB_TYPE = os.environ.get("AUTH_SERVER_DB_TYPE", default=None)
if ENV_SERVER_DB_TYPE is not None:
    server_config["database"]["volatile"]["type"] = ENV_SERVER_DB_TYPE

ENV_SERVER_DB_HOST = os.environ.get("AUTH_SERVER_DB_HOST", default=None)
if ENV_SERVER_DB_HOST is not None:
    server_config["database"]["volatile"]["host"] = ENV_SERVER_DB_HOST

ENV_SERVER_DB_PORT = os.environ.get("AUTH_SERVER_DB_PORT", default=None)
if ENV_SERVER_DB_PORT is not None:
    server_config["database"]["volatile"]["port"] = ENV_SERVER_DB_PORT

ENV_SERVER_DB_DATABASE = os.environ.get(
    "AUTH_SERVER_DB_DATABASE", default=None)
if ENV_SERVER_DB_DATABASE is not None:
    server_config["database"]["volatile"]["database"] = ENV_SERVER_DB_DATABASE

ENV_SERVER_DB_USERNAME = os.environ.get(
    "AUTH_SERVER_DB_USERNAME", default=None)
if ENV_SERVER_DB_USERNAME is not None:
    server_config["database"]["volatile"]["username"] = ENV_SERVER_DB_USERNAME

ENV_SERVER_DB_PASSWORD = os.environ.get(
    "AUTH_SERVER_DB_PASSWORD", default=None)
if ENV_SERVER_DB_PASSWORD is not None:
    server_config["database"]["volatile"]["password"] = ENV_SERVER_DB_PASSWORD

ENV_SERVER_DB_EXPTIME = os.environ.get("AUTH_SERVER_DB_EXPTIME", default=None)
if ENV_SERVER_DB_EXPTIME is not None:
    server_config["database"]["volatile"]["expiration_time"] = ENV_SERVER_DB_EXPTIME

# Making sure that all required config variables are set
if server_config["app"]["secret_key"] == "":
    raise ValueError("No secret key set for Flask application")

# Apply the config to flask app
app.config['SECRET_KEY'] = server_config["app"]['secret_key']

# Don need slash at end
app.url_map.strict_slashes = False

#########################################
# CLIENT CONFIG
#########################################
_client_config_file = "etc/client-config.yaml"

# Default client configuration
client_configuration = {
    "username": "Admin",
    "password": "",
    "auth_server": {
        "host": "localhost",
        "port": 8888
    }
}

# If config file is provided for client, use that instead of default
if os.path.exists(_client_config_file):
    with open(_client_config_file, "r") as config_file:
        client_config = yaml.load(config_file)

# If environment variables provided for client, use that instead of default or file
ENV_CLIENT_USERNAME = os.environ.get("AUTH_CLIENT_USERNAME", default=None)
if ENV_CLIENT_USERNAME is not None:
    client_config["username"] = ENV_CLIENT_USERNAME

ENV_CLIENT_AUTHSERVER_HOST = os.environ.get(
    "AUTH_CLIENT_AUTHSERVER_HOST", default=None)
if ENV_CLIENT_AUTHSERVER_HOST is not None:
    client_config["auth_server"]["host"] = ENV_CLIENT_AUTHSERVER_HOST

ENV_CLIENT_AUTHSERVER_PORT = os.environ.get(
    "AUTH_CLIENT_AUTHSERVER_PORT", default=None)
if ENV_CLIENT_AUTHSERVER_PORT is not None:
    client_config["auth_server"]["port"] = ENV_CLIENT_AUTHSERVER_PORT

#########################################
# DATABASE
#########################################

#########################################
# ROUTES
#########################################
from app.routes import register
from app.routes import login
from app.routes import logout


#########################################
# HTTP ERRORS
#########################################


@app.errorhandler(404)
def not_found(error):
    return jsonify({"message": "Error, could not found!"}), 404


#########################################
# CLI
#########################################
from app.cli import cli


def run():
    args = cli.parse_args()

    if args.subcommand is None:
        cli.print_help()
    else:
        args.func(args)
