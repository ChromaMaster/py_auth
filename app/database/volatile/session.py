import base64
import pickle

import redis


class Session:
    def __init__(self, host, port=6379, password="", default_ex=1800, db=0):
        self.r = redis.StrictRedis(
            host=host,
            port=port,
            db=db,
            password=password)
        self.default_ex = default_ex

    def start(self, key, fields):
        """ Starts a session identified by the key that stores a dict 'fields'
            The value of the key will be stored in base64
        """
        print(base64.b64encode(pickle.dumps(fields)))
        self.r.set(key, base64.b64encode(
            pickle.dumps(fields)), ex=self.default_ex)

    def status(self, key):
        """ Returns if session identified by the key exists """
        return self.r.exists(key)

    def data(self, key):
        """ Returns the data attached to the session """
        # data = self.r.hgetall(key)
        data = pickle.loads(base64.b64decode(self.r.get(key)))
        return data

    def reset(self, key):
        """ Resets the session expiration time"""
        self.r.expire(key, self.default_ex)

    def abort(self, key):
        """ Aborts the session identified by the key """
        return self.r.delete(key) > 0
