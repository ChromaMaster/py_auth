from app import server_config
from .session import Session

session = None


#########################################
# DATABASE CORE
#########################################


def create_connection():
    global session

    database_config = server_config["database"]["volatile"]
    session = Session(
        host=database_config['host'],
        port=database_config['port'],
        password=database_config['password'],
        default_ex=database_config['expiration_time'],
        db=database_config['database']
    )


#########################################
# DATA TREATMENT
#########################################


def create_user(token, user):
    """ Creates a new user session in the database """
    session.start(user.public_id, {"user": user.as_dict(), "token": token})


def remove_user(public_id):
    """ Removes the user's session from the database """
    if not session.abort(public_id):
        return False
    return True


def get_user(public_id):
    """ Get a user session from the database """
    return session.data(public_id)
