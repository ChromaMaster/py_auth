# Copyright (C) 2018  ChromaMaster

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app import server_config
from .model.base import Base
from .model.user import User

engine = None
session = None


#########################################
# DATABASE CORE
#########################################


def create_connection():
    """ Creates the engine to connect to the database """
    global engine

    database_config = server_config["database"]["persistent"]

    if database_config["type"] == "sqlite":
        uri = "sqlite:///{}".format(os.path.join(
            os.environ["BASE_PATH"], database_config["storage"]))
    elif database_config["type"] == "mysql":
        uri = "mysql://{username}:{password}@{host}:{port}/{database}".format(
            **database_config)
    elif (database_config["type"] == "postrgre"):
        uri = "postgresql://{username}:{password}@{host}:{port}/{database}".format(
            **database_config)
    else:
        raise ValueError("Database type {} not supported".format(
            database_config["type"]))

    engine = create_engine(str(uri), echo=False)


def initial_migration():
    """ Create all the database tables according to the models """
    if engine:
        Base.metadata.bind = engine
        Base.metadata.create_all(engine)
    else:
        raise RuntimeError("Database connection not created")


def connect():
    """ Connect to the database, creates the session if not exists """
    global session
    if session is None:
        Session = sessionmaker(bind=engine)
        session = Session()


def close():
    """ Close the database connection """
    global session
    session.close()
    session = None


#########################################
# DATA TREATMENT
#########################################


def import_users(users):
    """ Import users to the database """
    connect()
    for user in users:
        session.add(user)
    session.commit()
    close()


def create_user(user):
    """ Creates a new user and store it in the database """
    connect()
    session.add(user)
    session.commit()
    close()


def get_user_by_username(username):
    """ Returns a user from the dabase identified by the username """
    connect()
    user = session.query(User).filter_by(username=username).first()
    close()
    return user


def remove_user(user):
    """ Removes a user from the database """
    connect()
    session.delete(user)
    close()
