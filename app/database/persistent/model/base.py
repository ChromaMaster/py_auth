# Copyright (C) 2018  ChromaMaster

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime

from sqlalchemy import Column, Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base


class BaseModel(object):
    """ Base model for other database models to inherit """

    id = Column(Integer, primary_key=True)
    date_created = Column(DateTime, default=datetime.now)
    date_modified = Column(
        DateTime, default=datetime.now, onupdate=datetime.now)

    def as_dict(self):
        tmp = self.__dict__
        tmp.pop('_sa_instance_state', None)
        return tmp


Base = declarative_base(cls=BaseModel)
