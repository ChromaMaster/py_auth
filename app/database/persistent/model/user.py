# Copyright (C) 2018  ChromaMaster

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from sqlalchemy import Column, String, Boolean, Integer

from app.database.persistent.model.base import Base


class User(Base):
    """ User model to map a database table """
    __tablename__ = "users"

    public_id = Column(String(50), unique=True)
    username = Column(String(50), unique=True)
    password = Column(String(256))
    role = Column(Integer, default=5)

    def __init__(self, **kwargs):
        self.public_id = kwargs["public_id"]
        self.username = kwargs["username"]
        self.password = kwargs["password"]
        self.role = kwargs["role"]
